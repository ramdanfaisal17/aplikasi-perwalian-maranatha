<?php


Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
  Route::post('details', 'UserController@details');
});

Route::group( ['namespace' => 'Api'], function () {
  Route::resource('mahasiswa', 'MahasiswaController');
  
  Route::get('indexkeluarga/{id}','MahasiswaController@indexkeluarga');
  Route::get('indexberkas/{id}', 'MahasiswaBerkaController@indexberkas');

  Route::get('getprovinsi', 'MahasiswaController@getprovinsi');
  Route::get('getkota', 'MahasiswaController@getkota');
  Route::get('getkecamatan', 'MahasiswaController@getkecamatan');
  Route::get('getdesa', 'MahasiswaController@getdesa');

  Route::resource('dosen', 'DosenController');
  Route::resource('kela', 'KelaController');
  Route::resource('matakuliah', 'MataKuliahController');
  Route::resource('programstudi', 'ProgramStudiController');
  Route::resource('proyekpendidikan', 'ProyekPendidikanController');
  Route::resource('mahasiswakeluarga','MahasiswaKeluargaController');

  Route::get('indexkeluarga/mahasiswakeluarga/{id}','MahasiswaKeluargaController@creates');

  Route::post('/mahasiswaberkas/upload', 'MahasiswaBerkaController@proses_upload');
  Route::get('/mahasiswaberkas/delete/{id}', 'MahasiswaBerkaController@hapus');
});