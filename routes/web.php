<?php

//Route::get('/', function () {
//    return view('home');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('dosen', 'DosenController');
Route::post('dosen/deletes', 'DosenController@deletes')->name('dosen.Deletes');
Route::post('dosen/import', 'DosenController@import')->name('dosen.import');
Route::get('/data/dosen', 'DosenController@dataTable_for_show_data')->name('data.dosen');
Route::get('/data/dosen/show', 'DosenController@show_data')->name('data.dosen.show');
Route::get('get_dosen', 'DosenController@GetDosen');


Route::resource('user', 'UsersController');
Route::post('user/deletes', 'UsersController@deletes')->name('user.Deletes');
Route::post('user/import', 'UsersController@import')->name('user.import');

Route::resource('kelas', 'KelaController');
Route::post('kelas/deletes', 'KelaController@deletes')->name('kelas.deletes');
Route::post('kelas/import', 'KelaController@import')->name('kelas.import');


Route::resource('programstudi', 'ProgramStudiController');
Route::post('programstudi/deletes', 'ProgramStudiController@deletes')->name('programstudi.deletes');
Route::post('programstudi/import', 'ProgramStudiController@import')->name('programstudi.import');


Route::resource('proyekpendidikan', 'ProyekPendidikanController');
Route::post('proyekpendidikan/deletes', 'ProyekPendidikanController@deletes')->name('proyekpendidikan.deletes');
Route::post('proyekpendidikan/import', 'ProyekPendidikanController@import')->name('proyekpendidikan.import');


Route::resource('matakuliah', 'MataKuliahController');
Route::post('matakuliah/deletes', 'MataKuliahController@deletes')->name('matakuliah.deletes');
Route::post('matakuliah/import', 'MataKuliahController@import')->name('matakuliah.import');


Route::resource('mahasiswa', 'MahasiswaController');
Route::post('mahasiswa/deletes', 'MahasiswaController@deletes')->name('mahasiswa.Deletes');
Route::post('mahasiswa/import', 'MahasiswaController@import')->name('mahasiswa.import');


Route::get('getkota','MahasiswaController@getkota');
Route::get('getkecamatan','MahasiswaController@getkecamatan');
Route::get('getdesa','MahasiswaController@getdesa');
Route::get('indexkeluarga/{id}','MahasiswaController@indexkeluarga');

Route::resource('mahasiswakeluarga','MahasiswaKeluargaController');
Route::get('indexkeluarga/mahasiswakeluarga/{id}','MahasiswaKeluargaController@creates');
Route::post('mahasiswakeluarga/deletes', 'MahasiswaKeluargaController@deletes')->name('mahasiswakeluarga.Deletes');


Route::get('indexberkas/{id}', 'MahasiswaBerkaController@indexberkas');
Route::post('/mahasiswaberkas/upload', 'MahasiswaBerkaController@proses_upload')->name('mahasiswaberkas.upload');
Route::get('/mahasiswaberkas/delete/{id}', 'MahasiswaBerkaController@hapus')->name('mahasiswaberkas.delete');

Route::resource('jadwalPerwalian', 'jadwalPerwalianController');
Route::post('jadwalPerwalian/deletes', 'jadwalPerwalianController@deletes')->name('jadwalPerwalian.deletes');
// Route::post('jadwalPerwalian/import', 'jadwalPerwalianController@import')->name('jadwalPerwalian.import');
