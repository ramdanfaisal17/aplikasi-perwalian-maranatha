<?php

namespace Add\Imports;

use Add\Models\user;
use Add\Models\dosen;
use Add\Models\mahasiswa;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class UserImport implements ToModel
{

    public function model(array $row)
    {

        if ($row[3]=='Dosen'){
             $dosen = dosen::create([
                'nik'=> $row[1],
                'nama' => $row[2],
                'email' => $row[1].'@email.com',
            ]);
        }

        if ($row[3]=='Mahasiswa'){
             $mahasiswa = mahasiswa::create([
                'nrp'=> $row[1],
                'nama' => $row[2],
                'email' => $row[1].'@email.com',
            ]);
        }

        return new user([

            'nik'=> $row[1],
            'name' => $row[2],
            'relasi' => $row[3],
            'password' => Hash::make('maranatha'),
            'email' => $row[1].'@email.com',
        ]);


    }
}

