<header class="app-header navbar bg-primary">
        <div class="col-md-2 text-md-center">
            <i style="color: white ; font-size: 20px;">M a r a n a t h a</i> 
            
        </div>
        <div class="col-md-2 text-md-left" style="margin-left: -30px;" >
            <a href="/home" style="text-decoration: none;" class="sidebar-toggler" data-toggle="sidebar-lg-show">
                
                <i class="fa fa-bars" style="color: white ; font-size: 16px;"></i>  
            </a>
        </div>
        
        <ul class="nav navbar-nav ml-auto mr-3" >
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img class="img-avatar mx-1" src="{{Auth::user()->avatar_url}}">
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow mt-2">
                    <a class="dropdown-item">
                        {{ Auth::user()->name }}<br>
                        <small class="text-muted">{{ Auth::user()->email }}</small>
                    </a>
                    <a class="dropdown-item" href="/profile">
                        <i class="fas fa-user"></i> Profile
                    </a>
                    <div class="divider"></div>
                    <a class="dropdown-item" href="/password">
                        <i class="fas fa-key"></i> Password
                    </a>
                    <div class="divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </header>