<div class="sidebar" style="background-color:#002865;>
    <nav class="sidebar-nav ps ps--active-y">

        <ul class="nav">
            {{-- <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>Dashboard
                </a>
            </li> --}}
            

            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-users nav-icon"></i>Master
                </a>
                <ul class="nav-dropdown-items">
{{--                     <li class="nav-item">
                        <a href="{{ route("admin.products.index") }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}"><i class="fas fa-cogs nav-icon"></i>product
                        </a>
                    </li> --}}
                    @if (Auth::user()->name == 'admin')
                    <li class="nav-item">
                        <a href="{{ route("user.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>User
                        </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a href="{{ route("dosen.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>Dosen
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("kelas.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>kelas
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("programstudi.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>program studi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("proyekpendidikan.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>proyek pendidikan
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("matakuliah.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>mata kuliah
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("mahasiswa.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>mahasiswa
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-users nav-icon"></i>Transaksi
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("jadwalPerwalian.index") }}" class="nav-link">
                            <i class="fas fa-cogs nav-icon"></i>Jadwal Perwalian
                        </a>
                    </li>
                    
                </ul>
            </li>

            
{{--             <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();"><i class="nav-icon fas fa-sign-out-alt"></i>logout
                </a>
            </li> --}}
        </ul>

        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
    </nav>
    {{-- <button class="sidebar-minimizer brand-minimizer" type="button"></button> --}}
</div>