@extends('layouts.admin')
@section('content')

<div class="card" style="margin-top: -15px;">
    <div class="card-body">

        <form action="{{ route('jadwalPerwalian.update', [$jadwalperwalian->id]) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kode_perwalian">Kode Perwalian</label>
                                <input type="text" id="kode_perwalian" name="kode_perwalian" class="form-control" required autocomplete="off" autofocus value="{{$jadwalperwalian->kode_perwalian}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="proyek_pendidikan_id">Proyek Pendidikan</label>
                                <select name="proyek_pendidikan_id" id="proyek_pendidikan_id" class="form-control" value="{{ $jadwalperwalian->proyekpendidikan->nama}}">
                                    <option value="" selected disabled>pilih proyek pendidikan</option>
                                    @foreach($proyek_pendidikans as $key => $proyekpendidikan)
                                    <option value="{{$proyekpendidikan->id}}"> {{$proyekpendidikan->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="start_prs">1. Perubahan Rencana Studi (PRS)</label>
                                <input type="date" id="start_prs" class="form-control" name="start_prs" autocomplete="off" value="{{ $jadwalperwalian->start_prs}}">
                             </div>
                             <div class="form-group">
                                    <label for="start_konsultasi1">2. Konsultasi 1</label>
                                    <input type="date" id="start_konsultasi1" class="form-control" name="start_konsultasi1" autocomplete="off" value="{{ $jadwalperwalian->start_konsultasi1}}">
                                 </div>
                                 <div class="form-group">
                                        <label for="start_konsultasi2">3. Konsultasi 2</label>
                                        <input type="date" id="start_konsultasi2" class="form-control" name="start_konsultasi2" autocomplete="off" value="{{ $jadwalperwalian->start_konsultasi2}}">
                                 </div>
                                 <div class="form-group">
                                        <label for="start_antara">4. Untuk semester Depan (Antara)</label>
                                        <input type="date" id="start_antara" class="form-control" name="start_antara" autocomplete="off" value="{{ $jadwalperwalian->start_antara}}">
                                 </div>
                                  <div class="form-group">
                                        <label for="start_ganjil">5. Untuk semester Depan (Ganjil)</label>
                                        <input type="date" id="start_ganjil" class="form-control" name="start_ganjil" autocomplete="off" value="{{ $jadwalperwalian->start_ganjil}}">
                                 </div>
                                  <div class="form-group">
                                        <label for="start_genap">6. Untuk semester Depan (Genap)</label>
                                        <input type="date" id="start_genap" class="form-control" name="start_genap" autocomplete="off" value="{{ $jadwalperwalian->start_genap}}">
                                 </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input type="date" id="tanggal" name="tanggal" class="form-control" autocomplete="off" value="{{ $jadwalperwalian->tanggal}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="program_studi_id">Program Studi</label>
                                <select name="program_studi_id" id="program_studi_id" class="form-control">
                                    <option value="" selected disabled>pilih program studi</option>
                                    @foreach($programstudis as $key => $programstudi)
                                    <option value="{{$programstudi->id}}"> {{$programstudi->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                             <div class="form-group">
                                <label for="end_prs">End</label>
                                <input type="date" id="end_prs" class="form-control" name="end_prs" autocomplete="off" value="{{ $jadwalperwalian->end_prs}}">
                             </div>
                             <div class="form-group">
                                <label for="end_konsultasi1">End</label>
                                <input type="date" id="end_konsultasi1" class="form-control" name="end_konsultasi1" autocomplete="off" value="{{ $jadwalperwalian->end_konsultasi1}}">
                             </div>
                             <div class="form-group">
                                <label for="end_konsultasi2">End</label>
                                <input type="date" id="end_konsultasi2" class="form-control" name="end_konsultasi2" autocomplete="off" value="{{ $jadwalperwalian->end_konsultasi2}}">
                             </div>
                             <div class="form-group">
                                <label for="end_antara">End</label>
                                <input type="date" id="end_antara" class="form-control" name="end_antara" autocomplete="off" value="{{ $jadwalperwalian->end_antara}}">
                             </div>
                             <div class="form-group">
                                <label for="end_ganjil">End</label>
                                <input type="date" id="end_ganjil" class="form-control" name="end_ganjil" autocomplete="off" value="{{ $jadwalperwalian->end_ganjil}}">
                             </div>
                             <div class="form-group">
                                <label for="end_genap">End</label>
                                <input type="date" id="end_genap" class="form-control" name="end_genap" autocomplete="off" value="{{ $jadwalperwalian->end_genap}}">
                             </div>
                        </div>  
                    <div>
                <a href="{{ route('jadwalPerwalian.index') }}">
                    <input class="btn btn-warning" type="button" value="back">
                </a>
                <input class="btn btn-success" type="submit" value="save">
            </div>
        </form>




    </div>
</div>

@endsection

@section('scripts')


@endsection