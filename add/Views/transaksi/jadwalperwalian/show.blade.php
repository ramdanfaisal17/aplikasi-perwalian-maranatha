@extends('layouts.admin')
@section('content')


<div class="card">
    <div class="card-header">Jadwal Perwalian</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Kode Perwalian</th>
                    <td>{{ $jadwalperwalian->kode_perwalian }}</td>
                </tr>
                
                <tr>
                    <th>Proyek Pendidikan</th>
                    <td>{{ $jadwalperwalian->proyekpendidikan->nama}}</td>
                </tr>
                 <tr>
                    <th>Tanggal</th>
                    <td>{{ $jadwalperwalian->tanggal}}</td>
                </tr>
                 <tr>
                    <th>Program Studi</th>
                    <td>{{ $jadwalperwalian->ProgramStudi->nama}}</td>
                </tr>
                <tr>
                    <th>PRS Start</th>
                    <td>{{ $jadwalperwalian->start_prs}}</td>
                </tr>
                <tr>
                    <th>PRS END</th>
                    <td>{{ $jadwalperwalian->end_prs }}</td>
                </tr>
                <tr>
                    <th>Konsultasi 1 Start</th>
                    <td>{{ $jadwalperwalian->start_konsultasi1 }}</td>
                </tr>
                <tr>
                    <th>Konsultasi 1 END</th>
                    <td>{{ $jadwalperwalian->end_konsultasi1 }}</td>
                </tr>
                <tr>
                    <th>Konsultasi 2 Start</th>
                    <td>{{ $jadwalperwalian->start_konsultasi2 }}</td>
                </tr>
                <tr>
                    <th>Konsultasi 2 END</th>
                    <td>{{ $jadwalperwalian->end_konsultasi2 }}</td>
                </tr>
                <tr>
                    <th>Antara Start</th>
                    <td>{{ $jadwalperwalian->start_antara }}</td>
                </tr>
                <tr>
                    <th>Antara END</th>
                    <td>{{ $jadwalperwalian->end_antara }}</td>
                </tr>
                <tr>
                    <th>Ganjil Start</th>
                    <td>{{ $jadwalperwalian->start_ganjil }}</td>
                </tr>
                <tr>
                    <th>Ganjil END</th>
                    <td>{{ $jadwalperwalian->end_ganjil }}</td>
                </tr>
                <tr>
                    <th>Genap Start</th>
                    <td>{{ $jadwalperwalian->start_genap }}</td>
                </tr>
                <tr>
                    <th>Genap END</th>
                    <td>{{ $jadwalperwalian->end_genap }}</td>
                </tr>

            </tbody>
        </table>
        <a href="{{ route('jadwalPerwalian.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection