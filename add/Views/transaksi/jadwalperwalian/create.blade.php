@extends('layouts.admin')
@section('content')
<?php use Add\Controllers\JadwalPerwalianController;?>
<div class="card">
    <div class="card-header">New Jadwal Perwalian</div>
    <div class="card-body">
        <form action="{{ route("jadwalPerwalian.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="kode_perwalian">kode perwalian</label>
                            <input type="text" class="form-control" name="kode_perwalian" readonly value="<?php echo JadwalPerwalianController::autonumber(); ?>">
                        </div>
                           
                            <div class="form-group">
                                <label for="proyek_pendidikan_id">Proyek Pendidikan</label>
                                <select name="proyek_pendidikan_id" id="proyek_pendidikan_id" class="form-control">
                                    <option value="" selected disabled>pilih proyek pendidikan</option>
                                    @foreach($proyek_pendidikans as $key => $proyekpendidikan)
                                    <option value="{{$proyekpendidikan->id}}"> {{$proyekpendidikan->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                             <div class="form-group">
                                <label for="start_prs">1. Perubahan Rencana Studi (PRS)</label>
                                <input type="date" id="start_prs" class="form-control" name="start_prs" autocomplete="off">
                             </div>
                             <div class="form-group">
                                    <label for="start_konsultasi1">2. Konsultasi 1</label>
                                    <input type="date" id="start_konsultasi1" class="form-control" name="start_konsultasi1" autocomplete="off">
                                 </div>
                                 <div class="form-group">
                                        <label for="start_konsultasi2">3. Konsultasi 2</label>
                                        <input type="date" id="start_konsultasi2" class="form-control" name="start_konsultasi2" autocomplete="off">
                                 </div>
                                 <div class="form-group">
                                        <label for="start_antara">4. Untuk semester Depan (Antara)</label>
                                        <input type="date" id="start_antara" class="form-control" name="start_antara" autocomplete="off">
                                 </div>
                                  <div class="form-group">
                                        <label for="start_ganjil">5. Untuk semester Depan (Ganjil)</label>
                                        <input type="date" id="start_ganjil" class="form-control" name="start_ganjil" autocomplete="off">
                                 </div>
                                  <div class="form-group">
                                        <label for="start_genap">6. Untuk semester Depan (Genap)</label>
                                        <input type="date" id="start_genap" class="form-control" name="start_genap" autocomplete="off">
                                 </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input type="date" id="tanggal" name="tanggal" class="form-control" autocomplete="off" value="<?php echo date('Y-m-d'); ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label for="program_studi_id">Program Studi</label>
                                <select name="program_studi_id" id="program_studi_id" class="form-control">
                                    <option value="" selected disabled>pilih program studi</option>
                                    @foreach($programstudis as $key => $programstudi)
                                    <option value="{{$programstudi->id}}"> {{$programstudi->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                             <div class="form-group">
                                <label for="end_prs">End</label>
                                <input type="date" id="end_prs" class="form-control" name="end_prs" autocomplete="off">
                             </div>
                             <div class="form-group">
                                <label for="end_konsultasi1">End</label>
                                <input type="date" id="end_konsultasi1" class="form-control" name="end_konsultasi1" autocomplete="off">
                             </div>
                             <div class="form-group">
                                <label for="end_konsultasi2">End</label>
                                <input type="date" id="end_konsultasi2" class="form-control" name="end_konsultasi2" autocomplete="off">
                             </div>
                             <div class="form-group">
                                <label for="end_antara">End</label>
                                <input type="date" id="end_antara" class="form-control" name="end_antara" autocomplete="off">
                             </div>
                             <div class="form-group">
                                <label for="end_ganjil">End</label>
                                <input type="date" id="end_ganjil" class="form-control" name="end_ganjil" autocomplete="off">
                             </div>
                             <div class="form-group">
                                <label for="end_genap">End</label>
                                <input type="date" id="end_genap" class="form-control" name="end_genap" autocomplete="off">
                             </div>
                        </div>  
                    </div>
                    <input class="btn btn-success" type="submit" value="save All">
                </div>
        </form>
    </div>
</div>

@endsection
<script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
        </script>