@extends('layouts.admin')
@section('content')

@foreach($mahasiswas as $mahasiswa)@endforeach

<form action="{{ route('mahasiswaberkas.upload') }}" method="POST" enctype="multipart/form-data" hidden>
	@csrf
	<input type="file" name="files" id="files" >	
	<input type="text" name="file" id="file" >	
	<input type="text" name="mahasiswa_id" id="mahasiswa_id" value="{{$mahasiswa->id}}">
	<input type="text" name="keterangan" id="keterangan">
	<input type="submit" value="Upload" class="btn btn-primary" id="submit">
</form>

<div style="margin-bottom: 10px;" class="row">
	<div class="col-md-12">	

		<a class="btn btn-warning" href="{{ route("mahasiswa.index") }}">back</a>
		
	</div>
</div>


<div class="card">
	<div class="card-header">data berkas {{$mahasiswa->nama}}</div>
	<div class="card-body">
		<div class="row " style="min-height: 150px;">
			<div class="col-md-3 text-md-center">
				@foreach ($fotos as $foto)
				<img src="{{url('data_file', $foto->file) }}" class="img-responsive img-fluid poto" width="300" height="400" onclick="modalshow('{{$foto->file}}')" >
				@endforeach	
			</div>

			<div class="col-md-3 text-md-center">
				@foreach ($fotoktps as $fotoktp)
				<img src="{{url('data_file', $fotoktp->file) }}" class="img-responsive img-fluid poto" width="300" height="400" onclick="modalshow('{{$fotoktp->file}}')" >
				@endforeach	

			</div>

			<div class="col-md-3 text-md-center">
				@foreach ($fotoktpayahs as $fotoktpayah)
				<img src="{{url('data_file', $fotoktpayah->file) }}" class="img-responsive img-fluid poto" width="300" height="400" onclick="modalshow('{{$fotoktpayah->file}}')" >
				@endforeach	

			</div>

			<div class="col-md-3 text-md-center">
				@foreach ($fotoktpibus as $fotoktpibu)
				<img src="{{url('data_file', $fotoktpibu->file) }}" class="img-responsive img-fluid poto" width="300" height="400" onclick="modalshow('{{$fotoktpibu->file}}')" >
				@endforeach	
				
			</div>

		</div>

		<div class="row" style="margin-top: 10px;">
			<div class="col-md-3 text-md-center">
				<div class="btn-group">
					@foreach($fotos as $foto)
					@if($foto)
					<a onclick="return confirm('yakin di hapus?')" href="{{route('mahasiswaberkas.delete',$foto->id)}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
					@endif
					@endforeach
					<button class="btn btn-primary" onclick="uploadfoto()"><i class="fa fa-arrow-up"></i> upload Foto</button>
				</div>
			</div>
			<div class="col-md-3 text-md-center">
				<div class="btn-group">
					@foreach($fotoktps as $fotoktp)
					@if($fotoktp)
					<a onclick="return confirm('yakin di hapus?')" href="{{route('mahasiswaberkas.delete',$fotoktp->id)}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
					@endif
					@endforeach
					<button class="btn btn-primary" onclick="uploadfotoktp()"><i class="fa fa-arrow-up"></i> upload Foto ktp</button>
				</div>
			</div>
			<div class="col-md-3 text-md-center">
				<div class="btn-group">
					@foreach($fotoktpayahs as $fotoktpayah)
					@if($fotoktpayah)
					<a onclick="return confirm('yakin di hapus?')" href="{{route('mahasiswaberkas.delete',$fotoktpayah->id)}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
					@endif
					@endforeach
					<button class="btn btn-primary" onclick="uploadfotoktpayah()"><i class="fa fa-arrow-up"></i> upload Foto ktp ayah</button>
				</div>
			</div>
			<div class="col-md-3 text-md-center">
				<div class="btn-group">
					@foreach($fotoktpibus as $fotoktpibu)
					@if($fotoktpibu)
					<a onclick="return confirm('yakin di hapus?')" href="{{route('mahasiswaberkas.delete',$fotoktpibu->id)}}" class="btn btn-danger" ><i class="fa fa-times"></i></a>
					@endif
					@endforeach
					<button class="btn btn-primary" onclick="uploadfotoktpibu()"><i class="fa fa-arrow-up"></i> upload Foto ktp ibu</button>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 10px;min-height: 150px;">
			<div class="col-md-3 text-md-center">
				@foreach ($fotokks as $fotokk)
				<img src="{{url('data_file', $fotokk->file) }}" class="img-responsive img-fluid poto" width="300" height="400" onclick="modalshow('{{$fotoktpkk->file}}')" >
				@endforeach	
			</div>
		</div>
		<div class="row" style="margin-top: 10px;">
			<div class="col-md-3 text-md-center">
				<div class="btn-group">
					@foreach($fotokks as $fotokk)
					@if($fotokk)
					<a onclick="return confirm('yakin di hapus?')" href="{{route('mahasiswaberkas.delete',$fotokk->id)}}" class="btn btn-danger"><i class="fa fa-times"></i></a>
					@endif
					@endforeach
					<button class="btn btn-primary" onclick="uploadfotokk()"><i class="fa fa-arrow-up"></i> upload Foto kk</button>
				</div>
			</div>
		</div>
	</div>


</div>

</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img class="img-responsive img-fluid" id="modalimage">
			</div>

		</div>
	</div>
</div>


<style>
.poto{
	padding: 10px;
	transition: 0.5s;
	
}
.poto:hover{
	cursor: pointer;
	padding: 0px;
	transition: 0.5s;
}
</style>

@endsection

@section('scripts')
<script>

	function modalshow(data){
		$("#modalimage").attr("src","/data_file/"+data);
		$('#myModal').modal('show');
	}

	id = $('#mahasiswa_id').val();

	function uploadfoto(){
		$('#file').val('foto----'+id+'.png');
		$('#keterangan').val('foto');
		$('#files').click();
	}

	function uploadfotoktp(){
		$('#file').val('fotoktp----'+id+'.png');
		$('#keterangan').val('fotoktp');
		$('#files').click();
	}

	function uploadfotoktpayah(){
		$('#file').val('fotoktpayah----'+id+'.png');
		$('#keterangan').val('fotoktpayah');
		$('#files').click();
	}

	function uploadfotoktpibu(){
		$('#file').val('fotoktpibu----'+id+'.png');
		$('#keterangan').val('fotoktpibu');
		$('#files').click();
	}

	function uploadfotokk(){
		$('#file').val('fotokk----'+id+'.png');
		$('#keterangan').val('fotokk');
		$('#files').click();
	}

	$('#files').change(function(){
		if (confirm('ganti dengan file ini ?')) {
			$('#submit').click();
		}
	})

</script>
@endsection


