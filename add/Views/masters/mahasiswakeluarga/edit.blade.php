@extends('layouts.admin')
@section('content')

@foreach($mahasiswas as $mahasiswa)@endforeach

<div class="card">
    <div class="card-header">edit keluarga {{ $mahasiswa->nama }}</div>

    <div class="card-body">
        <form action="{{ route("mahasiswakeluarga.update", [$mahasiswakeluarga->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col">

                    <div class="form-group" hidden>
                        <label for="mahasiswa_id">mahasiswa_id</label>
                        <input type="text" id="mahasiswa_id" name="mahasiswa_id" class="form-control" value="{{ $mahasiswa->id }}">
                    </div>
                    <div class="form-group">
                        <label for="hubungan">hubungan</label>
                        <select name="hubungan" id="hubungan" class="form-control">
                            <option selected value="{{$mahasiswakeluarga->hubungan}}">{{$mahasiswakeluarga->hubungan}}</option>
                            <option value="Ayah">Ayah</option>
                            <option value="Ibu">Ibu</option>
                            <option value="Kakak">Kakak</option>
                            <option value="Adik">Adik</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama">nama</label>
                        <input type="text" id="nama" name="nama" class="form-control" value="{{ $mahasiswakeluarga->nama }}">
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir">tanggal_lahir</label>
                        <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control" value="{{ $mahasiswakeluarga->tanggal_lahir }}">
                    </div>
                    <div class="form-group">
                        <label for="tempat_lahir">tempat_lahir</label>
                        <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" value="{{ $mahasiswakeluarga->tempat_lahir }}">
                    </div>

                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="alamat_pekerjaan">alamat_pekerjaan</label>
                        <input type="text" id="alamat_pekerjaan" name="alamat_pekerjaan" class="form-control" value="{{ $mahasiswakeluarga->alamat_pekerjaan }}">
                    </div>
                    <div class="form-group">
                        <label for="pendidikan_terkahir">pendidikan_terkahir</label>
                        <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control">
                            <option selected value="{{$mahasiswakeluarga->pendidikan_terakhir}}">{{$mahasiswakeluarga->pendidikan_terakhir}}</option>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA">SMA</option>
                            <option value="D3">D3</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="no_hp">no_hp</label>
                        <input type="text" id="no_hp" name="no_hp" class="form-control" value="{{ $mahasiswakeluarga->no_hp }}">
                    </div>
                    <div class="form-group">
                        <label for="email">email</label>
                        <input type="email" id="email" name="email" class="form-control" value="{{ $mahasiswakeluarga->email }}">
                    </div>
                </div>
            </div>
            <div>
                <a href="/indexkeluarga/{{$mahasiswakeluarga->mahasiswa_id }}">
                    <input type="button" class="btn btn-warning" value="back">
                </a>
                <input class="btn btn-success" type="submit" value="save">
            </div>

        </form>
    </div>
</div>

@endsection