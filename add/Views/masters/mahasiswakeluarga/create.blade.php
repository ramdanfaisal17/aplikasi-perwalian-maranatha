@extends('layouts.admin')
@section('content')

@foreach($mahasiswas as $mahasiswa)@endforeach

<div class="card">
    <div class="card-header">tambah keluarga  {{$mahasiswa->nama}}</div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">

                <form action="{{route('mahasiswakeluarga.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group"  hidden>
                        <label for="mahasiswa_id">mahasiswa_id</label>
                        <input type="text" id="mahasiswa_id" name="mahasiswa_id" class="form-control" value="{{$id}}">
                    </div>
                    <div class="form-group">
                        <label for="hubungan">hubungan</label>
                        <select name="hubungan" id="hubungan" class="form-control">
                            <option disbaled selected value="-">pilih</option>
                            <option value="Ayah">Ayah</option>
                            <option value="Ibu">Ibu</option>
                            <option value="Kakak">Kakak</option>
                            <option value="Adik">Adik</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama">nama</label>
                        <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir">tanggal_lahir</label>
                        <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control" required autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="tempat_lahir">tempat_lahir</label>
                        <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" required autocomplete="off">
                    </div>

                </div>
            
            
                <div class="col-md-6">


                    <div class="form-group">
                        <label for="alamat_pekerjaan">alamat_pekerjaan</label>
                        <input type="text" id="alamat_pekerjaan" name="alamat_pekerjaan" class="form-control" required autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="pendidikan_terakhir">pendidikan_terakhir</label>
                        <select name="pendidikan_terakhir" id="pendidikan_terakhir" class="form-control">
                            <option disabled selected value="-">pilih</option>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA">SMA</option>
                            <option value="D3">D3</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="no_hp">no_hp</label>
                        <input type="text" id="no_hp" name="no_hp" class="form-control" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="email">email</label>
                        <input type="email" id="email" name="email" class="form-control" autocomplete="off">
                    </div>
                </div>
            </div>

            <div>
                <input class="btn btn-success" type="submit" value="Create">
            </div>
        </form>
    </div>
</div>

@endsection