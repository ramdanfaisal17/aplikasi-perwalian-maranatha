@extends('layouts.admin')
@section('content')
@foreach($mahasiswas as $mahasiswa)@endforeach

<div class="card">
    <div class="card-header">keluarga  {{ $mahasiswa->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>hubungan</th>
                    <td>{{ $mahasiswakeluarga->hubungan }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $mahasiswakeluarga->nama}}</td>
                </tr>
                <tr>
                    <th>tanggal lahir</th>
                    <td>{{ $mahasiswakeluarga->tanggal_lahir}}</td>
                </tr>
                <tr>
                    <th>tempat lahir</th>
                    <td>{{ $mahasiswakeluarga->tempat_lahir}}</td>
                </tr>
                <tr>
                    <th>alamat pekerjaan</th>
                    <td>{{ $mahasiswakeluarga->alamat_pekerjaan}}</td>
                </tr>
                <tr>
                    <th>pendidikan terakhir</th>
                    <td>{{ $mahasiswakeluarga->pendidikan_terakhir}}</td>
                </tr>
                <tr>
                    <th>no hp</th>
                    <td>{{ $mahasiswakeluarga->no_hp}}</td>
                </tr>
                <tr>
                    <th>email</th>
                    <td>{{ $mahasiswakeluarga->email }}</td>
                </tr>
            </tbody>
        </table>
        <a href="/indexkeluarga/{{$mahasiswakeluarga->mahasiswa_id }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection