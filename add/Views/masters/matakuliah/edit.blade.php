@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">edit matakuliah {{ $matakuliah->nama }}</div>

    <div class="card-body">
        <form action="{{ route("matakuliah.update", [$matakuliah->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="kode">kode</label>
                <input type="text" id="kode" name="kode" class="form-control" value="{{ $matakuliah->kode }}">
            </div>
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" id="nama" name="nama" class="form-control" value="{{ $matakuliah->nama }}">
            </div>
            <div class="form-group">
                <label for="sks">sks</label>
                <input type="text" id="sks" name="sks" class="form-control" value="{{ $matakuliah->sks }}">
            </div>
            <div class="form-group">
                <label for="prasyarat">prasyarat</label>
                <input type="text" id="prasyarat" name="prasyarat" class="form-control" value="{{ $matakuliah->prasyarat }}">
            </div>
            <div class="form-group">
                <label for="semester">semester</label>
                <input type="number" id="semester" name="semester" class="form-control" value="{{ $matakuliah->semester }}">
            </div>
            <div class="form-group">
                <label for="sifat">sifat</label>
                <input type="text" id="sifat" name="sifat" class="form-control" value="{{ $matakuliah->sifat }}">
            </div>
            <div>
                <a href="{{ route('matakuliah.index') }}">
                    <input class="btn btn-warning" type="button" value="back">
                </a>
                <input class="btn btn-success" type="submit" value="save">
            </div>
        </form>
    </div>
</div>

@endsection