@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">matakuliah  {{ $model->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>kode</th>
                    <td>{{ $model->kode }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $model->nama}}</td>
                </tr>
                <tr>
                    <th>sks</th>
                    <td>{{ $model->sks}}</td>
                </tr>
                <tr>
                    <th>prasyarat</th>
                    <td>{{ $model->prasyarat}}</td>
                </tr>
                <tr>
                    <th>semester</th>
                    <td>{{ $model->semester}}</td>
                </tr>
                <tr>
                    <th>sifat</th>
                    <td>{{ $model->sifat}}</td>
                </tr>
            </tbody>
        </table>
        <h3>Data Dosen </h3>   
        <div class="col-md-12">
            <table class="table table-hovered">
              <thead>
                <th>Nik</th>
                <th>Nama</th>
                <th>Email</th>
              </thead>
              @foreach ($matkuldetail as $item)
              <tbody>

                <td>{{$item->nik}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->email}}</td>
              </tbody>
              @endforeach
            </table>
        </div>

        <a href="{{ route('matakuliah.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>


</div>

@endsection