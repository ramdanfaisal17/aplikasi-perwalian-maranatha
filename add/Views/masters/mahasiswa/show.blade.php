@extends('layouts.admin')
@section('content')


<div class="card">
    <div class="card-header">Mahasiswa  {{ $mahasiswa->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>nrp</th>
                    <td>{{ $mahasiswa->nrp }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $mahasiswa->nama}}</td>
                </tr>
                <tr>
                    <th>tanggal_lahir</th>
                    <td>{{ $mahasiswa->tanggal_lahir }}</td>
                </tr>
                <tr>
                    <th>tempat_lahir</th>
                    <td>{{ $mahasiswa->tempat_lahir }}</td>
                </tr>
                <tr>
                    <th>jenis kelamin</th>
                    <td>{{ $mahasiswa->jenis_kelamin }}</td>
                </tr>
                <tr>
                    <th>agama</th>
                    <td>{{ $mahasiswa->agama }}</td>
                </tr>

                <tr>
                    <th>email</th>
                    <td>{{ $mahasiswa->email }}</td>
                </tr>

                <tr>
                    <th>no hp</th>
                    <td>{{ $mahasiswa->no_hp }}</td>
                </tr>
                <tr>
                    <th>alamat</th>
                    <td>{{ $mahasiswa->alamat }}</td>
                </tr>
                <tr>
                    <th>provinsi</th>
                    @foreach($provinsi as $prov)
                    <td>{{ $prov->name }}</td>}
                    @endforeach
                </tr>
                <tr>
                    <th>kota</th>
                    @foreach($kota as $kot)
                    <td>{{ $kot->name }}</td>
                    @endforeach
                </tr>
                <tr>
                    <th>kecamatan</th>
                    @foreach($kecamatan as $kec)
                    <td>{{ $kec->name }}</td>
                    @endforeach
                </tr>
                <tr>
                    <th>desa</th>
                    @foreach($desa as $des)
                    <td>{{ $des->name }}</td>
                    @endforeach
                </tr>

                <tr>
                    <th>kode pos</th>
                    <td>{{ $mahasiswa->kode_pos }}</td>
                </tr>
                <tr>
                    <th>warga negara</th>
                    <td>{{ $mahasiswa->warga_negara }}</td>
                </tr>
                <tr>
                    <th>golongan darah</th>
                    <td>{{ $mahasiswa->golongan_darah }}</td>
                </tr>
                <tr>
                    <th>rhesus</th>
                    <td>{{ $mahasiswa->rhesus }}</td>
                </tr>
                <tr>
                    <th>no ijazah</th>
                    <td>{{ $mahasiswa->no_ijazah }}</td>
                </tr>
                <tr>
                    <th>tanggal ijazah</th>
                    <td>{{ $mahasiswa->tanggal_ijazah }}</td>
                </tr>
                <tr>
                    <th>nem</th>
                    <td>{{ $mahasiswa->nem }}</td>
                </tr>
                <tr>
                    <th>nilai toefel</th>
                    <td>{{ $mahasiswa->nilai_toefel }}</td>
                </tr>
                <tr>
                    <th>jalur masuk</th>
                    <td>{{ $mahasiswa->jalur_masuk }}</td>
                </tr>
                <tr>
                    <th>status mahasiswa</th>
                    <td>{{ $mahasiswa->status_mahasiswa }}</td>
                </tr>


            </tbody>
        </table>
        <a href="{{ route('mahasiswa.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection