@extends('layouts.admin')
@section('content')

<div class="card" style="margin-top: -15px;">
    <div class="card-header" >new Mahasiswa</div>
    <div class="card-body">


        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-personal-info-tab" data-toggle="pill" href="#pills-personal-info" role="tab" aria-controls="pills-personal-info" aria-selected="true">Personal info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-alamat-tab" data-toggle="pill" href="#pills-alamat" role="tab" aria-controls="pills-alamat" aria-selected="false">Address</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-detail-info-tab" data-toggle="pill" href="#pills-detail-info" role="tab" aria-controls="pills-detail-info" aria-selected="false">Detail info</a>
            </li>
        </ul>

        <form action="{{ route("mahasiswa.store")}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="tab-content" id="pills-tabContent" style="margin-bottom: 10px;">
{{-- tab1 --}}
                <div class="tab-pane fade show active" id="pills-personal-info" role="tabpanel" aria-labelledby="pills-personal-info-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nrp">nrp</label>
                                <input type="number" id="nrp" name="nrp" class="form-control" required autocomplete="off" autofocus>
                            </div>
                            <div class="form-group">
                                <label for="nama">nama</label>
                                <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="tanggal_lahir">tanggal lahir</label>
                                <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir">tempat lahir</label>
                                <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" required autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">jenis kelamin</label>
                                <div class="form-control">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="jenis_kelamin" id="jenis_kelamin1" value="laki-laki" checked>
                                        <label class="custom-control-label" for="jenis_kelamin1">laki-laki</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="jenis_kelamin" id="jenis_kelamin2" value="perempuan">
                                        <label class="custom-control-label" for="jenis_kelamin2">perempuan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="agama">agama</label>
                                <select name="agama" id="agama" class="form-control">
                                    <option disabled selected>pilih agama</option>
                                    <option value="budha">budha</option>
                                    <option value="hindu">hindu</option>
                                    <option value="islam">islam</option>
                                    <option value="katolik">katolik</option>
                                    <option value="protestan">protestan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">email</label>
                                <input type="email" id="email" name="email" class="form-control" required autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="no_hp">no hp</label>
                                <input type="text" id="no_hp" name="no_hp" class="form-control" required autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
{{-- tab2 --}}
                <div class="tab-pane fade" id="pills-alamat" role="tabpanel" aria-labelledby="pills-alamat-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="alamat">alamat</label>
                                <textarea name="alamat" id="alamat" cols="30" rows="8" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="warga_negara">warga negara</label>
                                <input type="text" id="warga_negara" name="warga_negara" class="form-control" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="provinsi_id">provinsi</label>
                                <select name="provinsi_id" id="provinsi_id" class="form-control">
                                    <option value="" selected disabled>pilih provinsi</option>
                                    @foreach($provinsis as $key => $provinsi)
                                    <option value="{{$provinsi->id}}"> {{$provinsi->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kota_id">kota</label>
                                <select name="kota_id" id="kota_id" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <label for="kecamatan_id">kecamatan</label>
                                <select name="kecamatan_id" id="kecamatan_id" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label for="desa_id">desa</label>
                                <select name="desa_id" id="desa_id" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <label for="kode_pos">kode pos</label>
                                <input type="number" id="kode_pos" name="kode_pos" class="form-control" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
{{-- tab3 --}}
                <div class="tab-pane fade" id="pills-detail-info" role="tabpanel" aria-labelledby="pills-detail-info-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="golongan_darah">golongan darah</label>
                                <select name="golongan_darah" id="golongan_darah" class="form-control">
                                    <option value="-" selected disabled>pilih</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="AB">AB</option>
                                    <option value="O">O</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="rhesus">rhesus</label>
                                <input type="text" id="rhesus" name="rhesus" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="no_ijazah">no ijazah</label>
                                <input type="text" id="no_ijazah" name="no_ijazah" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="tanggal_ijazah">tanggal ijazah</label>
                                <input type="date" id="tanggal_ijazah" name="tanggal_ijazah" class="form-control" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="nem">nem</label>
                                <input type="number" id="nem" name="nem" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="nilai_toefel">nilai toefel</label>
                                <input type="number" id="nilai_toefel" name="nilai_toefel" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="jalur_masuk">jalur masuk</label>
                                <input type="text" id="jalur_masuk" name="jalur_masuk" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="status_mahasiswa">status mahasiswa</label>
                                <input type="text" id="status_mahasiswa" name="status_mahasiswa" class="form-control" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

            <input class="btn btn-success" type="submit" value="save All">
        </form>




    </div>
</div>

@endsection

@section('scripts')
<script>

   $(function () {
    $('#myTab li:last-child a').tab('show');
});

   $('#provinsi_id').on('change',function(){
    var province_id = $(this).val();

    $.ajax({
     url:"{{url('getkota')}}?province_id="+province_id,
     type:"GET",
     success:function(res){               
        if(res){
            $("#kota_id").empty();
            $("#kota_id").append('<option disabled selected>pilih kota</option>');
            $.each(res,function(key,value){
                $("#kota_id").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kota_id").empty();
          $("#kecamatan_id").empty();
          $("#desa_id").empty();
      } 
  }
});
});

   $('#kota_id').on('change',function(){
    var city_id = $(this).val();

    $.ajax({
     url:"{{url('getkecamatan')}}?city_id="+city_id,
     type:"GET",
     success:function(res){               
        if(res){
            $("#kecamatan_id").empty();
            $("#kecamatan_id").append('<option disabled selected>pilih kecamatan</option>');
            $("#kecamatan_id").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#kecamatan_id").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kecamatan_id").empty();
          $("#desa_id").empty();
      } 
  }
});
});

   $('#kecamatan_id').on('change',function(){
    var district_id = $(this).val();

    $.ajax({
     url:"{{url('getdesa')}}?district_id="+district_id,
     type:"GET",
     success:function(res){               
        if(res){
            $("#desa_id").empty();
            $("#desa_id").append('<option disabled selected>pilih desa</option>');
            $("#desa_id").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#desa_id").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#desa_id").empty();
      } 
  }
});
});

</script>

@endsection