@extends('layouts.admin')
@section('content')

<div class="card" style="margin-top: -15px;">
    <div class="card-header" >edit Mahasiswa {{$mahasiswa->nama}}</div>
    <div class="card-body">


        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-personal-info-tab" data-toggle="pill" href="#pills-personal-info" role="tab" aria-controls="pills-personal-info" aria-selected="true">Personal info</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-alamat-tab" data-toggle="pill" href="#pills-alamat" role="tab" aria-controls="pills-alamat" aria-selected="false">Address</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-detail-info-tab" data-toggle="pill" href="#pills-detail-info" role="tab" aria-controls="pills-detail-info" aria-selected="false">Detail info</a>
            </li>
        </ul>

        <form action="{{ route("mahasiswa.update", [$mahasiswa->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="tab-content" id="pills-tabContent" style="margin-bottom: 10px;">
                {{-- tab1 --}}
                <div class="tab-pane fade show active" id="pills-personal-info" role="tabpanel" aria-labelledby="pills-personal-info-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nrp">nrp</label>
                                <input type="number" id="nrp" name="nrp" class="form-control" required autocomplete="off" autofocus value="{{$mahasiswa->nrp}}">
                            </div>
                            <div class="form-group">
                                <label for="nama">nama</label>
                                <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off" value="{{$mahasiswa->nama}}">
                            </div>
                            <div class="form-group">
                                <label for="tanggal_lahir">tanggal lahir</label>
                                <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control" required autocomplete="off" value="{{$mahasiswa->tanggal_lahir}}">
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir">tempat lahir</label>
                                <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" required autocomplete="off" value="{{$mahasiswa->tempat_lahir}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">jenis kelamin</label>

                                <div class="form-control">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="jenis_kelamin" id="jenis_kelamin1" value="laki-laki" checked>
                                        <label class="custom-control-label" for="jenis_kelamin1">laki-laki</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="jenis_kelamin" id="jenis_kelamin2" value="perempuan">
                                        <label class="custom-control-label" for="jenis_kelamin2">perempuan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="agama">agama</label>
                                <select name="agama" id="agama" class="form-control">
                                    <option disabled selected>pilih agama</option>
                                    <option value="budha">budha</option>
                                    <option value="hindu">hindu</option>
                                    <option value="islam">islam</option>
                                    <option value="katolik">katolik</option>
                                    <option value="protestan">protestan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">email</label>
                                <input type="email" id="email" name="email" class="form-control" required autocomplete="off"value="{{$mahasiswa->email}}">
                            </div>

                            <div class="form-group">
                                <label for="no_hp">no hp</label>
                                <input type="text" id="no_hp" name="no_hp" class="form-control" required autocomplete="off" value="{{$mahasiswa->no_hp}}">
                            </div>
                        </div>
                    </div>
                </div>
                {{-- tab2 --}}
                <div class="tab-pane fade" id="pills-alamat" role="tabpanel" aria-labelledby="pills-alamat-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="alamat">alamat</label>
                                <textarea name="alamat" id="alamat" cols="30" rows="8" class="form-control" value="{{$mahasiswa->alamat}}"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="warga_negara">warga negara</label>
                                <input type="text" id="warga_negara" name="warga_negara" class="form-control" autocomplete="off" value="{{$mahasiswa->warga_negara}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="provinsi">provinsi</label>
                                <select name="provinsi" id="provinsi" class="form-control">
                                    <option value="" selected disabled>pilih provinsi</option>
                                    @foreach($provinsi as $prov)
                                    <option value="{{$prov->id}}" selected>{{$prov->name}}</option>
                                    @endforeach
                                    @foreach($provinsis as $key => $provinsi)
                                    <option value="{{$provinsi->id}}"> {{$provinsi->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="kota">kota</label>
                                <select name="kota" id="kota" class="form-control">
                                    @foreach($kota as $kot)
                                    <option value="{{$kot->id}}" selected>{{$kot->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="kecamatan">kecamatan</label>
                                <select name="kecamatan" id="kecamatan" class="form-control">
                                    @foreach($kecamatan as $kec)
                                    <option value="{{$kec->id}}" selected>{{$kec->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="desa">desa</label>
                                <select name="desa" id="desa" class="form-control">
                                    @foreach($desa as $des)
                                    <option value="{{$des->id}}" selected>{{$des->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="kode_pos">kode pos</label>
                                <input type="number" id="kode_pos" name="kode_pos" class="form-control" autocomplete="off" value="{{$mahasiswa->kode_pos}}">
                            </div>
                        </div>
                    </div>
                </div>
                {{-- tab3 --}}
                <div class="tab-pane fade" id="pills-detail-info" role="tabpanel" aria-labelledby="pills-detail-info-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="golongan_darah">golongan darah</label>
                                <select name="golongan_darah" id="golongan_darah" class="form-control">
                                    <option value="-" selected disabled>pilih</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="AB">AB</option>
                                    <option value="O">O</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="rhesus">rhesus</label>
                                <input type="text" id="rhesus" name="rhesus" class="form-control" autocomplete="off" value="{{$mahasiswa->rhesus}}">
                            </div>
                            <div class="form-group">
                                <label for="no_ijazah">no ijazah</label>
                                <input type="text" id="no_ijazah" name="no_ijazah" class="form-control" autocomplete="off" value="{{$mahasiswa->no_ijazah}}">
                            </div>
                            <div class="form-group">
                                <label for="tanggal_ijazah">tanggal ijazah</label>
                                <input type="date" id="tanggal_ijazah" name="tanggal_ijazah" class="form-control" autocomplete="off" value="{{$mahasiswa->tanggal_ijazah}}">
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="nem">nem</label>
                                <input type="number" id="nem" name="nem" class="form-control" autocomplete="off" value="{{$mahasiswa->nem}}">
                            </div>
                            <div class="form-group">
                                <label for="nilai_toefel">nilai toefel</label>
                                <input type="number" id="nilai_toefel" name="nilai_toefel" class="form-control" autocomplete="off" value="{{$mahasiswa->nilai_toefel}}">
                            </div>
                            <div class="form-group">
                                <label for="jalur_masuk">jalur masuk</label>
                                <input type="text" id="jalur_masuk" name="jalur_masuk" class="form-control" autocomplete="off" value="{{$mahasiswa->jalur_masuk}}">
                            </div>
                            <div class="form-group">
                                <label for="status_mahasiswa">status mahasiswa</label>
                                <input type="text" id="status_mahasiswa" name="status_mahasiswa" class="form-control" autocomplete="off" value="{{$mahasiswa->status_mahasiswa}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

            <input class="btn btn-success" type="submit" value="save All">
        </form>




    </div>
</div>

@endsection

@section('scripts')
<script>

   $(function () {
    $('#myTab li:last-child a').tab('show');
});

   $('#provinsi').on('change',function(){
    var province_id = $(this).val();

    $.ajax({
     url:"{{url('getkota')}}?province_id="+province_id,
     type:"GET",
     success:function(res){               
        if(res){
            $("#kota").empty();
            $("#kota").append('<option disabled selected>pilih kota</option>');
            $.each(res,function(key,value){
                $("#kota").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kota").empty();
          $("#kecamatan").empty();
          $("#desa").empty();
      } 
  }
});
});

   $('#kota').on('change',function(){
    var city_id = $(this).val();

    $.ajax({
     url:"{{url('getkecamatan')}}?city_id="+city_id,
     type:"GET",
     success:function(res){               
        if(res){
            $("#kecamatan").empty();
            $("#kecamatan").append('<option disabled selected>pilih kecamatan</option>');
            $("#kecamatan").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#kecamatan").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kecamatan").empty();
          $("#desa").empty();
      } 
  }
});
});

   $('#kecamatan').on('change',function(){
    var district_id = $(this).val();

    $.ajax({
     url:"{{url('getdesa')}}?district_id="+district_id,
     type:"GET",
     success:function(res){               
        if(res){
            $("#desa").empty();
            $("#desa").append('<option disabled selected>pilih desa</option>');
            $("#desa").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#desa").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#desa").empty();
      } 
  }
});
});

</script>

@endsection