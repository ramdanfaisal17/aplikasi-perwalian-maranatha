@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">programstudi  {{ $programstudi->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>kode</th>
                    <td>{{ $programstudi->kode }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $programstudi->nama}}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('programstudi.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection