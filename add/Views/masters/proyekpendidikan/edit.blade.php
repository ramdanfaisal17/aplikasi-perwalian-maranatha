@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">edit proyekpendidikan {{ $proyekpendidikan->nama }}</div>

    <div class="card-body">
        <form action="{{ route("proyekpendidikan.update", [$proyekpendidikan->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="kode">kode</label>
                <input type="text" id="kode" name="kode" class="form-control" value="{{ $proyekpendidikan->kode }}">
            </div>
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" id="nama" name="nama" class="form-control" value="{{ $proyekpendidikan->nama }}">
            </div>

            <div>
                <a href="{{ route('proyekpendidikan.index') }}">
                    <input class="btn btn-warning" type="button" value="back">
                </a>
                <input class="btn btn-success" type="submit" value="save">
            </div>
        </form>
    </div>
</div>

@endsection