@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">proyekpendidikan  {{ $proyekpendidikan->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>kode</th>
                    <td>{{ $proyekpendidikan->kode }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $proyekpendidikan->nama}}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('proyekpendidikan.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection