@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">user  {{ $user->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>nik</th>
                    <td>{{ $user->nik }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $user->name}}</td>
                </tr>
                <tr>
                    <th>jabatan</th>
                    <td>{{ $user->relasi }}</td>
                </tr>
                <tr>
                    <th>email</th>
                    <td>{{ $user->email }}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('user.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection