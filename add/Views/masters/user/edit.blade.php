@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">edit user {{ $user->name }}</div>

    <div class="card-body">
        <form action="{{ route("user.update", [$user->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="nik" class="col-md-4 col-form-label text-md-right">nik  /  nrp</label>

                <div class="col-md-6">
                    <input id="nik" type="number" class="form-control @error('nik') is-invalid @enderror" name="nik" required autofocus value="{{$user->nik}}">

                    @error('nik')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">nama</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" required autocomplete="name" >

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="relasi" class="col-md-4 col-form-label text-md-right">jabatan</label>
                <div class="col-md-6">
                    <select name="relasi" id="relasi" class="form-control">
                        <option disabled selected value="{{$user->relasi}}">{{$user->relasi}}</option>
                        <option value="Dosen">Dosen</option>
                        <option value="Mahasiswa">Mahasiswa</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">email</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" required autocomplete="name" >

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div>
                <a href="{{ route('user.index') }}">
                    <input class="btn btn-warning" type="button" value="back">
                </a>
                <input class="btn btn-success" type="submit" value="save">
            </div>
        </form>
    </div>
</div>

@endsection