@extends('layouts.admin')
@section('content')


<div class="card">
    <div class="card-header">new user</div>
    <div class="card-body">
        <form method="POST" action="{{ route('user.store') }}">
            @csrf

            <div class="form-group row">
                <label for="nik" class="col-md-4 col-form-label text-md-right">nik  /  nrp</label>

                <div class="col-md-6">
                    <input id="nik" type="number" class="form-control @error('nik') is-invalid @enderror" name="nik" required autofocus>

                    @error('nik')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">nama</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" >

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="relasi" class="col-md-4 col-form-label text-md-right">jabatan</label>
                <div class="col-md-6">
                    <select name="relasi" id="relasi" class="form-control">
                        <option disabled selected value="-">pilih</option>
                        <option value="Dosen">Dosen</option>
                        <option value="Mahasiswa">Mahasiswa</option>
                    </select>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        register
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection