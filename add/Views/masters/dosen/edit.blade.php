@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">edit dosen {{ $dosen->nama }}</div>

    <div class="card-body">
        <form action="{{ route("dosen.update", [$dosen->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nik">nik</label>
                <input type="number" id="nik" name="nik" class="form-control" value="{{ $dosen->nik }}">
            </div>
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" id="nama" name="nama" class="form-control" value="{{ $dosen->nama }}">
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="email" id="email" name="email" class="form-control" value="{{ $dosen->email }}">
            </div>

            <div>
                <a href="{{ route('dosen.index') }}">
                    <input class="btn btn-warning" type="button" value="back">
                </a>
                <input class="btn btn-success" type="submit" value="save">
            </div>
        </form>
    </div>
</div>

@endsection