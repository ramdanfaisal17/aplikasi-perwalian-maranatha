@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">new Dosen</div>
    <div class="card-body">
        <form action="{{ route("dosen.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="nik">nik</label>
                <input type="number" id="nik" name="nik" class="form-control" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="email" id="email" name="email" class="form-control" required autocomplete="off">
            </div>

            <div>
                <input class="btn btn-success" type="submit" value="Create">
            </div>
        </form>
    </div>
</div>

@endsection