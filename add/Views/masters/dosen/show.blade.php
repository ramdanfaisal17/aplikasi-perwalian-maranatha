@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">Dosen  {{ $dosen->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>nik</th>
                    <td>{{ $dosen->nik }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $dosen->nama}}</td>
                </tr>
                <tr>
                    <th>email</th>
                    <td>{{ $dosen->email }}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('dosen.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection