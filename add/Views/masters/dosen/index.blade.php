@extends('layouts.admin')
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>dosen</h3>
</div>
<div class="col-lg-8 text-md-right">
    <button class="btn btn-danger" onclick="$('.dtb-delete').click();" style="min-width: 100px;">
      delete Selected
  </button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#importExcel" style="min-width: 100px;">
      import Excel
  </button>
  <button type="button" class="btn btn-warning" onclick="$('.dtb-excel').click();" style="min-width:100px;">
      export Excel
  </button>
  <a class="btn btn-success" href="{{ route("dosen.create") }}" style="min-width: 100px;">
      create New
  </a>



  {{-- notifikasi form validasi --}}
  @if ($errors->has('file'))
  <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('file') }}</strong>
  </span>
  @endif

  {{-- notifikasi sukses --}}
  @if ($sukses = Session::get('sukses'))
  <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $sukses }}</strong>
  </div>
  @endif
  <!-- Import Excel -->
  <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('dosen.import') }}" enctype="multipart/form-data">

          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
          </div>
          <div class="modal-body">
              @csrf
              <label>Pilih file excel</label>
              <div class="form-group">
                <input type="file" name="file" required="required">
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Import</button>
      </div>
  </div>
</form>
</div>
</div>

</div>
</div>

<div class="card">

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table datatable" id="mytable">
                <thead>
                    <tr>
                        <th width="10">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2"></label>
                        </div>
                    </th>
                    <th width="100">action</th>
                    <th>nik</th>
                    <th>nama</th>
                    <th>email</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dosens as $item)
                <tr data-entry-id="{{ $item->id }}">
                    <td></td>
                    <td>
                        <form action="{{ route('dosen.destroy', $item->id) }}" method="POST" onsubmit="return confirm('yakin nih?');" style="display: inline-block;">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-xs btn-danger" value="X">
                        </form>
                        <a class="btn btn-xs btn-primary" href="{{ route('dosen.edit', $item->id) }}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-success" href="{{ route('dosen.show', $item->id) }}">
                            <i class="fa fa-check"></i>
                        </a>
                    </td>
                    <td>{{ $item->nik }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->email }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "dosen/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });

</script>

@endsection
@endsection
