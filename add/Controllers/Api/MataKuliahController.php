<?php

namespace Add\Controllers\api;

use Add\Models\matakuliah;
use Illuminate\Http\Request;
use Add\Controllers\Controller;


class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matakuliahs = matakuliah::orderby('created_at','desc')->get();
        return response()->json($matakuliahs);
    }

    public function store(Request $request)
    {
        $matakuliah = matakuliah::create($request->all());
        return response()->json($matakuliah);
        
    }

    public function update(Request $request, matakuliah $matakuliah)
    {
        $matakuliah->update($request->all());
        return response()->json($matakuliah);

    }

    public function show(matakuliah $matakuliah)
    {
        $show = matakuliah::where('id',$matakuliah->id)->get();
       return response()->json($show);
   }

   public function destroy($id)
   {
    $matakuliah = matakuliah::where('id',$id)->delete();
    return response()->json('delete success');
}

public function massDestroy(Request $request)
{

}


}
