<?php

namespace Add\Controllers\api;

use Add\Models\dosen;
use Illuminate\Http\Request;
use Add\Controllers\Controller;


class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosens = dosen::orderby('created_at','desc')->get();
        return response()->json($dosens);
    }

    public function store(Request $request)
    {
        $dosen = dosen::create($request->all());
        return response()->json($dosen);
        
    }

    public function update(Request $request, dosen $dosen)
    {
        $dosen->update($request->all());
        return response()->json($dosen);

    }

    public function show(dosen $dosen)
    {
        $show = dosen::where('id',$dosen->id)->get();
       return response()->json($show);
   }

   public function destroy($id)
   {
    $dosen = dosen::where('id',$id)->delete();
    return response()->json('delete success');
}

public function massDestroy(Request $request)
{

}


}
