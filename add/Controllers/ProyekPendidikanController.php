<?php

namespace Add\Controllers;

use Add\Models\proyekpendidikan;
use Illuminate\Http\Request;
use Add\Imports\ProyekPendidikanImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProyekPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyekpendidikans = proyekpendidikan::orderby('kode','desc')->get();
        return view('masters.proyekpendidikan.index',compact('proyekpendidikans'));
    }

    public function import(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_proyekpendidikan',$nama_file);

        // import data
        Excel::import(new ProyekPendidikanImport, public_path('/file_proyekpendidikan/'.$nama_file));

        // notifikasi dengan session
        // Session::flash('sukses','Data Dosen Berhasil Diimport!');

        // alihkan halaman kembali
        //return redirect('/dosen');
        return back();
    }

    public function create()
    {
        return view('masters.proyekpendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proyekpendidikan = proyekpendidikan::create($request->all());
        return redirect()->route('proyekpendidikan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function show(proyekpendidikan $proyekpendidikan)
    {
        return view('masters.proyekpendidikan.show', compact('proyekpendidikan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function edit(proyekpendidikan $proyekpendidikan)
    {
        return view('masters.proyekpendidikan.edit', compact('proyekpendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, proyekpendidikan $proyekpendidikan)
    {
        $proyekpendidikan->update($request->all());

        return redirect()->route('proyekpendidikan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        proyekpendidikan::where('id',$id)->delete();
//        return redirect('/proyekpendidikan')->with('status','berhasil menghapus data');
        return back();

    }

    public function deletes(Request $request)
    {
        proyekpendidikan::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }


}
