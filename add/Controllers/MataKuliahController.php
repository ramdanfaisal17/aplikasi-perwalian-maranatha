<?php

namespace Add\Controllers;

use Add\Models\matakuliah;
use Add\Models\matakuliah_detail;
use Illuminate\Http\Request;
use Add\Imports\MataKuliahImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use DB;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matakuliahs = matakuliah::orderby('created_at','desc')->get();
        return view('masters.matakuliah.index',compact('matakuliahs'));
    }

    public function import(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_matakuliah',$nama_file);

        // import data
        Excel::import(new MataKuliahImport, public_path('/file_matakuliah/'.$nama_file));

        // notifikasi dengan session
        // Session::flash('sukses','Data Dosen Berhasil Diimport!');

        // alihkan halaman kembali
        //return redirect('/dosen');
        return back();
    }

    public function create()
    {
        return view('masters.matakuliah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $matakuliah = [
            'kode' => $request->input('kode'),
            'nama' => $request->input('nama'),
            'sks' => $request->input('sks'),
            'prasyarat' => $request->input('prasyarat'),
            'semester' => $request->input('semester'),
            'sifat' => $request->input('sifat')
        ];
            $creatematakuliah=matakuliah::create($matakuliah);
            $this->storedetail($request,$creatematakuliah);
            return redirect()->route('matakuliah.index');

    }

    public function storedetail($request,$creatematakuliah)
    {
         $matakuliahDetail = array();
        $idDosen = $request->iddetail;

        for($i = 0 ; $i < sizeof($idDosen); $i++){
        $matakuliahDetail[] = [
         'dosen_id' => $request->iddetail[$i],  
            'mata_kuliah_id' => $creatematakuliah->id
        ];
        }
         
    
          $idpmatakuliahDetail = array();
        for($i=0;$i < sizeof($matakuliahDetail);$i++)
        {
            $idpmatakuliahDetail[] = matakuliah_detail::create($matakuliahDetail[$i]);
        }

        if(sizeof($idpmatakuliahDetail) != sizeof($matakuliahDetail))
        {
            DB::rollback();
            return $res['status'] = 'Faild On Create Permintaan Pembelian Detail';
        }

        DB::commit();

         // return redirect()->route('matakuliah.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    // public function show(matakuliah $matakuliah)
    // {
    //     return view('masters.matakuliah.show', compact('matakuliah'));
    // }

     public function show($id)
    {
        $model = matakuliah::where('id',$id)->first();
        // $matkuldetail = matakuliah_detail::with('dosen')
        // // ->where('mata_kuliah_id', $id)
        // ->where('id', $model->id)
        //->get();
        $matkuldetail = matakuliah_detail::join('dosens', 'mata_kuliah_details.dosen_id', '=', 'dosens.id')
       ->where('mata_kuliah_id', $model->id)
        ->get();
        //$pp = PPembelian::where('idppembelian',$id)->get();
        return view('masters.matakuliah.show')
            ->with('model',$model)
            //->with('pp',$pp)
            ->with('matkuldetail',$matkuldetail);

    }   


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function edit(matakuliah $matakuliah)
    {
        return view('masters.matakuliah.edit', compact('matakuliah')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, matakuliah $matakuliah)
    {
        $matakuliah->update($request->all());
               
        $matkuldetail = matakuliah_detail::join('dosens', 'mata_kuliah_details.dosen_id', '=', 'dosens.id')
       ->where('mata_kuliah_id', $matakuliah->id)
        ->get();
        return view('masters.matakuliah.edit')
            ->with('model',$model)
            ->with('matkuldetail',$matkuldetail);

        //  $this->destroydetail($id);
        // $this->storedetail($request,$creatematakuliah);
        return redirect()->route('matakuliah.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        matakuliah::where('id',$id)->delete();
        // matakuliah_detail::where('mata_kuliah_id',$id)->delete();
//        return redirect('/matakuliah')->with('status','berhasil menghapus data');

         $this->destroydetail($id);
        return back();

    }

    public function destroydetail($id)
    {
      
        matakuliah_detail::where('mata_kuliah_id',$id)->delete();
//        return redirect('/matakuliah')->with('status','berhasil menghapus data')

    }



     public function deletes(Request $request)
    {
        matakuliah::whereIn('id', request('ids'))->delete();
        matakuliah_detail::whereIn('mata_kuliah_id', request('ids'))->delete();
        return response(null, 204);
    }
    
}
