<?php

namespace Add\Controllers;

use Add\Models\mahasiswa;
use Add\Models\mahasiswakeluarga;
use Add\Models\alamat\provinsi;
use Add\Models\alamat\kota;
use Add\Models\alamat\kecamatan;
use Add\Models\alamat\desa;
use Illuminate\Http\Request;
use Add\Imports\MahasiswaImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use DB;


class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswas = mahasiswa::orderby('created_at','desc')->get();
        return view('masters.mahasiswa.index',compact('mahasiswas'));
    }

    public function import(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_mahasiswa',$nama_file);

        // import data
        Excel::import(new MahasiswaImport, public_path('/file_mahasiswa/'.$nama_file));

        // notifikasi dengan session
        // Session::flash('sukses','Data Dosen Berhasil Diimport!');

        // alihkan halaman kembali
        //return redirect('/dosen');
        return back();
    }

    public function create()
    {
        $provinsis = provinsi::all();
        return view('masters.mahasiswa.create',compact('provinsis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mahasiswa = mahasiswa::create($request->all());
        return redirect()->route('mahasiswa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show(mahasiswa $mahasiswa)
    {
     $provinsi = provinsi::where('id',$mahasiswa->provinsi_id)->get();
     $kota = kota::where('id',$mahasiswa->kota_id)->get();
     $kecamatan = kecamatan::where('id',$mahasiswa->kecamatan_id)->get();
     $desa = desa::where('id',$mahasiswa->desa_id)->get();
     return view('masters.mahasiswa.show', compact('mahasiswa','provinsi','kota','kecamatan','desa'));
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(mahasiswa $mahasiswa)
    {
        $provinsis = provinsi::all();
        $provinsi = provinsi::where('id',$mahasiswa->provinsi_id)->get();
        $kota = kota::where('id',$mahasiswa->kota_id)->get();
        $kecamatan = kecamatan::where('id',$mahasiswa->kecamatan_id)->get();
        $desa = desa::where('id',$mahasiswa->desa_id)->get();
        return view('masters.mahasiswa.edit', compact('mahasiswa','provinsis','provinsi','kota','kecamatan','desa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, mahasiswa $mahasiswa)
    {
        $mahasiswa->update($request->all());

        return redirect()->route('mahasiswa.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        mahasiswa::where('id',$id)->delete();
//        return redirect('/mahasiswa')->with('status','berhasil menghapus data');
        return back();

    }

    public function massDestroy(Request $request)
    {

    }

    public function getkota(Request $request){

        $kota = DB::table("indonesia_cities")->where("province_id",$request->province_id)->pluck("name","id");
        return response()->json($kota);
    }

    public function getkecamatan(Request $request){

        $kecamatan = DB::table("indonesia_districts")->where("city_id",$request->city_id)->pluck("name","id");
        return response()->json($kecamatan);
    }

    public function getdesa(Request $request){

        $desa = DB::table("indonesia_villages")->where("district_id",$request->district_id)->pluck("name","id");
        return response()->json($desa);
    }

    public function indexkeluarga($id)
    {

        $mahasiswas = mahasiswa::where('id',$id)->get();
        $mahasiswakeluargas = mahasiswakeluarga::where('mahasiswa_id',$id)->get();
        return view('masters.mahasiswakeluarga.index',compact('mahasiswas','mahasiswakeluargas'));

    }

    public function indexberkas(){
        return view('masters.mahasiswaberkas.index');
    }

    public function deletes(Request $request)
    {
        mahasiswa::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }

}
