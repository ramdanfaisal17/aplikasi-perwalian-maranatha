<?php

namespace Add\Controllers;

use Add\Models\programstudi;
use Add\Imports\ProgramStudiImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProgramStudiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programstudis = programstudi::orderby('kode','desc')->get();
        return view('masters.programstudi.index',compact('programstudis'));
    }

    public function import(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_programstudi',$nama_file);

        // import data
        Excel::import(new ProgramStudiImport, public_path('/file_programstudi/'.$nama_file));

        // notifikasi dengan session
        // Session::flash('sukses','Data Dosen Berhasil Diimport!');

        // alihkan halaman kembali
        //return redirect('/dosen');
        return back();
    }

    public function create()
    {
        return view('masters.programstudi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $programstudi = programstudi::create($request->all());
        return redirect()->route('programstudi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function show(programstudi $programstudi)
    {
        return view('masters.programstudi.show', compact('programstudi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function edit(programstudi $programstudi)
    {
        return view('masters.programstudi.edit', compact('programstudi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, programstudi $programstudi)
    {
        $programstudi->update($request->all());

        return redirect()->route('programstudi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        programstudi::where('id',$id)->delete();
//        return redirect('/programstudi')->with('status','berhasil menghapus data');
        return back();

    }

   public function deletes(Request $request)
    {
        programstudi::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }


}
