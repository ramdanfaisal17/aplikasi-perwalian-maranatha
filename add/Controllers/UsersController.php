<?php

namespace Add\Controllers;

use Add\Models\user;
use Add\Models\dosen;
use Add\Models\mahasiswa;
use Illuminate\Http\Request;
use Add\Imports\userImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;






class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = user::orderby('created_at','desc')->get();
        return view('masters.user.index',compact('users'));
    }


    public function import(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_user',$nama_file);

        // import data
        Excel::import(new userImport, public_path('/file_user/'.$nama_file));

        // notifikasi dengan session
        // Session::flash('sukses','Data user Berhasil Diimport!');

        // alihkan halaman kembali
        //return redirect('/user');
        return back();
    }


    public function create()
    {
        return view('masters.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$user = user::create($request->all());

        $validator = $request->validate([
           'name' => ['required', 'string', 'max:255'],
           'nik' => ['required', 'string', 'min:6', 'unique:users'],
           'relasi' => ['required','string'],
       ]);

        try {

            if ($request['relasi']=='Dosen'){
                $dosen = dosen::create([
                    'nik' => $request['nik'],
                    'nama' => $request['name'],
                    'email' => $request['nik'].'@email.com',
                ]);
            }

            if ($request['relasi']=='Mahasiswa'){
                $mahasiswa = mahasiswa::create([
                    'nrp' => $request['nik'],
                    'nama' => $request['name'],
                    'email' => $request['nik'].'@email.com',
                ]);
            }

            $user = user::create([
                'name' => $request['name'],
                'email' => $request['nik'].'@email.com',
                'password' => Hash::make('maranatha'),
                'nik' => $request['nik'],
                'relasi' => $request['relasi'],
            ]);

            return redirect()->route('user.index');

        } catch (Exception $exc) {
            abort(404, $exc->getMessage());
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        return view('masters.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        return view('masters.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        $user->update($request->all());

        $validator = $request->validate([
           'name' => ['required', 'string', 'max:255'],
           'nik' => ['required', 'string', 'min:6', 'unique:users'],
           'relasi' => ['required','string'],
           'email' => ['required','string','unique:users'],
       ]);



        $user = user::update([
            'name' => $request['name'],
            'email' => $request['email'],
            'nik' => $request['nik'],
            'relasi' => $request['relasi'],
        ]);

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        user::where('id',$id)->delete();
//        return redirect('/user')->with('status','berhasil menghapus data');
        return back();

    }

    public function deletes(Request $request)
    {
        user::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }
}
