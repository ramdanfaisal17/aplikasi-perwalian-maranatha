<?php

namespace Add\Controllers;

use Add\Models\dosen;
use Illuminate\Http\Request;
use Add\Imports\DosenImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use DataTables;



class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosens = Dosen::orderby('created_at','desc')->get();
        return view('masters.dosen.index',compact('dosens'));
    }


    public function import(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_dosen',$nama_file);

        // import data
        Excel::import(new DosenImport, public_path('/file_dosen/'.$nama_file));

        // notifikasi dengan session
        // Session::flash('sukses','Data Dosen Berhasil Diimport!');

        // alihkan halaman kembali
        //return redirect('/dosen');
        return back();
    }


    public function create()
    {
        return view('masters.dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dosen = Dosen::create($request->all());
        return redirect()->route('dosen.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function show(dosen $dosen)
    {
        return view('masters.dosen.show', compact('dosen'));
    }
    
    public function show_data(){
        return view('masters.dosen.data');
    }

    public function dataTable_for_show_data(){

         $model = Dosen::all();
        return  DataTables::of($model)
                ->make(true);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function edit(dosen $dosen)
    {
        return view('masters.dosen.edit', compact('dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dosen $dosen)
    {
        $dosen->update($request->all());

        return redirect()->route('dosen.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dosen::where('id',$id)->delete();
//        return redirect('/dosen')->with('status','berhasil menghapus data');
        return back();

    }

    public function deletes(Request $request)
    {
        dosen::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }
}
