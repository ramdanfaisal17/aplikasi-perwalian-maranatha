<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class dosen extends Model
{
    protected $fillable = [
        'nik',
        'nama',
        'email',
    ];
}
