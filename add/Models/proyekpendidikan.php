<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class proyekpendidikan extends Model
{
  protected $table = 'proyek_pendidikans';

  protected $fillable = [
    'kode',
    'nama',
  ];
}
