<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class matakuliah extends Model
{
    protected $table = 'mata_kuliahs';
    protected $fillable = [
        'kode',
        'nama',
        'sks',
        'prasyarat',
        'semester',
        'sifat',
    ];
	protected $primaryKey = "id";


    public function matakuliahdetail()
    {
        return $this->hasMany('Add\Models\matakuliah_detail', 'id');
    }

   
}