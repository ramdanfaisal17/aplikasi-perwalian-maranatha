<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class programstudi extends Model
{
  protected $table = 'program_studis';

  protected $fillable = [
    'kode',
    'nama',
  ];
}
