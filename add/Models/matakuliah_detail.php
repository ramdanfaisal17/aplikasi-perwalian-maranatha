<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class matakuliah_detail extends Model
{
    protected $table = 'mata_kuliah_details';
    protected $fillable = [
        'mata_kuliah_id',
        'dosen_id',
    ];

    protected $primaryKey = "id";

    public function dosen()
    {
        return $this->hasMany('Add\Models\dosen', 'id');
    }

    public function matakuliah()
    {
        return $this->belongsTo('Add\Models\matakuliah', 'id');
    }
}


