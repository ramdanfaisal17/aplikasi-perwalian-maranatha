<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class jadwalperwalian extends Model
{
    protected $primaryKey='id_jadwal_perwalian';
      protected $table = 'jadwal_perwalian';
    protected $fillable = [
      'id_jadwal_perwalian',      
      'kode_perwalian',
      'proyek_pendidikan_id',
      'tanggal',
      'program_studi_id',
      'start_prs',
      'end_prs',
      'start_konsultasi1',
      'end_konsultasi1',
      'start_konsultasi2',
      'end_konsultasi2',
      'start_antara',
      'end_antara',
      'start_ganjil',
      'end_ganjil', 
      'start_genap',
       'end_genap', 
    ];

        public function ProgramStudi()
    {
        return $this->belongsto('Add\Models\ProgramStudi', 'program_studi_id');
    }

        public function proyekpendidikan()
    {
        return $this->belongsto('Add\Models\proyekpendidikan', 'proyek_pendidikan_id');
    }
}
