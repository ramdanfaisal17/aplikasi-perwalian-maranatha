<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class mahasiswa extends Model
{
    protected $fillable = [
        'nrp',
        'nama',
        'tanggal_lahir',
        'tempat_lahir',
        'jenis_kelamin',
        'agama',
        'email',
        'no_hp',
        'alamat',
        'provinsi_id',
        'kota_id',
        'kecamatan_id',
        'desa_id',
        'kode_pos',
        'warga_negara',
        'golongan_darah',
        'rhesus',
        'no_ijazah',
        'tanggal_ijazah',
        'nem',
        'nilai_toefel',
        'jalur_masuk',
        'status_mahasiswa',
    ];
}
