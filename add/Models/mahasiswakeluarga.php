<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class mahasiswakeluarga extends Model
{
    protected $table = 'mahasiswa_keluargas';
    protected $fillable = [
        'mahasiswa_id',
        'hubungan',
        'nama',
        'tanggal_lahir',
        'tempat_lahir',
        'alamat_pekerjaan',
        'pendidikan_terakhir',
        'no_hp',
        'email',
    ];
}
