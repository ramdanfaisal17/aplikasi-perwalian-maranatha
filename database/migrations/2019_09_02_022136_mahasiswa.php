<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mahasiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->bigIncrements('id');      
            $table->String('nrp')->nullable();
            $table->String('nama')->nullable();
            $table->Date('tanggal_lahir')->nullable();
            $table->String('tempat_lahir')->nullable();
            $table->String('jenis_kelamin')->default('laki-laki');
            $table->String('agama')->nullable();
            $table->String('email')->nullable();
            $table->String('no_hp')->nullable();
            $table->String('alamat')->nullable();
            $table->bigInteger('provinsi_id')->nullable();
            $table->bigInteger('kota_id')->nullable();
            $table->bigInteger('kecamatan_id')->nullable();
            $table->bigInteger('desa_id')->nullable();
            $table->String('kode_pos')->nullable();
            $table->String('warga_negara')->nullable();
            $table->String('golongan_darah')->nullable();
            $table->String('rhesus')->nullable();
            $table->String('no_ijazah')->nullable();
            $table->Date('tanggal_ijazah')->nullable();
            $table->String('nem')->nullable();
            $table->String('nilai_toefel')->nullable();
            $table->String('jalur_masuk')->nullable();
            $table->String('status_mahasiswa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
