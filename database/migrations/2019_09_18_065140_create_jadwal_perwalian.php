<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalPerwalian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_perwalian', function (Blueprint $table) {
            $table->bigIncrements('id_jadwal_perwalian');      
            $table->String('kode_perwalian')->nullable();
            $table->bigInteger('proyek_pendidikan_id')->nullable();
            $table->Date('tanggal')->nullable();
            $table->bigInteger('program_studi_id')->nullable();
            $table->Date('start_prs')->nullable();
            $table->Date('end_prs')->nullable();
            $table->Date('start_konsultasi1')->nullable();
            $table->Date('end_konsultasi1')->nullable();
            $table->Date('start_konsultasi2')->nullable();
            $table->Date('end_konsultasi2')->nullable();
            $table->Date('start_antara')->nullable();
            $table->Date('end_antara')->nullable();
            $table->Date('start_ganjil')->nullable();
            $table->Date('end_ganjil')->nullable(); 
            $table->Date('start_genap')->nullable();
            $table->Date('end_genap')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_perwalian');
    }
}
